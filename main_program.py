from random import randrange
from datetime import datetime


def starter_menu():
    """
    This function print starting menu
    """
    print(70 * '-')
    print('|                  Добро пожаловать в игру "Города"                  |\n'
          '|                 Чтобы начать игру введите: начать                  |\n'
          '|          Для ознакомления с правилами игры введите: правила        |\n'
          '| В любой момент вы можете написать "выход" для завершения программы |\n'
          '|                                        Автор игры: Artyom Kholopov |')
    print(70 * '-')


def controller_command():
    """
    This function check choice of player
    """
    while True:
        with open("logs.txt", 'a', encoding='utf-8') as logs:
            logs.write("{} Function controller_command was called\n".
                       format((datetime.now().strftime("%d-%m-%Y %H:%M"))))
        command = input('Команда: ')
        if command.lower() == 'выход':
            exit_from_game()
        if (command.lower() == 'правила') or (command.lower() == 'начать'):
            break
        else:
            print('Не нарушайте правила, введите "правила" или "начать" :)')
    return command


def rules():
    """
    This function print rules of game
    """
    print(83 * '-', end='\n'
          '|                              Правила игры в "Города"                            |\n'
          '| *Начинается игра с любого названия города.                                      |\n'
          '| *Каждый из участников игры по "кругу" называет реальный город, название         |\n'
          '|  которого начинается на букву, которой заканчивается предыдущее название города |\n'
          '| *Например: МосквА-АнапА-АгриджентО-ОмсК(и так далее)                            |\n'
          '| *Запрещается повторение городов.                                                |\n'
          '| *Названий городов на "ё", "ы", твердый "ъ" и мягкий "ъ" знак нет.               |\n'
          '| *Игра завершается когда игрок не в силах больше вспомнить название города.      |\n')
    print(83 * '-', end='\n')
    print('Надеюсь, правила понятны. Начнём? (введите "начать")')
    with open("logs.txt", 'a', encoding='utf-8') as logs:
        logs.write("{} Function rules was called\n".format((datetime.now().strftime("%d-%m-%Y %H:%M"))))


def first_town():
    """
    This function take a first town in game
    """
    global cities, used_cities, score_player
    print('Давайте начнём!')
    while True:
        town = input('Введите название города: ')
        town = town.lower()
        town = town.title()
        if town.lower() == 'выход':
            exit_from_game()
        with open("logs.txt", 'a', encoding='utf-8') as logs:
            logs.write("{} First word - {} \n".format((datetime.now().strftime("%d-%m-%Y %H:%M")), town))
        if town.lower() == 'города':
            print('Ещё не был введён ни один город.')
        if town in cities:
            print('Хорошо! Такой город существует.\n')
            cities.remove(town)
            used_cities.append(town)
            score_player += 1
            break
        else:
            print('Такого города не существует. Повторите попытку.')
    return find_last_letter(town)


def find_last_letter(word):
    """
    This function find last letter of word
    """
    global exceptions_symbols
    if word[-1] in exceptions_symbols:
        last_letter = word[-2]
    else:
        last_letter = word[-1]
    return last_letter


def next_word(last_letter):
    """
    This function print town of computer
    """
    global cities, used_cities, score_computer
    for town in cities:
        if town[0].lower() == last_letter:
            first_index = cities.index(town)
            break
    for town in cities:
        if town[0].lower() == chr(ord(last_letter) + 1):
            second_index = cities.index(town)
            break
    random_index = randrange(first_index, second_index)
    town = cities[random_index]
    print('Город компьютера: ' + town)
    print('Вам нужно написать город на букву "' + find_last_letter(town).upper() + '"')
    cities.remove(town)
    used_cities.append(town)
    score_computer += 1
    with open("logs.txt", 'a', encoding='utf-8') as logs:
        logs.write("{} Next town from computer - {} \n".format((datetime.now().strftime("%d-%m-%Y %H:%M")), town))
    return town


def player_move(last_letter):
    """
    This function take a town of player
    """
    global cities, used_cities, score_player
    town = input('Введите название города: ')
    town = town.lower()
    town = town.title()
    if town.lower() == 'выход':
        exit_from_game()
    if town.lower() == 'города':
        names_of_used_cities()
        return player_move(last_letter)
    with open("logs.txt", 'a', encoding='utf-8') as logs:
        logs.write("{} Next word from player - {} \n".format((datetime.now().strftime("%d-%m-%Y %H:%M")), town))
    if town in cities and last_letter == town[0].lower():
        print('Хорошо! Такой город существует.')
        cities.remove(town)
        used_cities.append(town)
        score_player += 1
        print()
    elif town in cities and last_letter != town[0].lower():
        print('Такой город существует, но он начинается не на последнюю букву предыдущего города.\n'
              'Повторите попытку.')
        return player_move(last_letter)
    else:
        print('Такого города не существует(возможно вы его уже называли). Повторите попытку.')
        return player_move(last_letter)
    return find_last_letter(town)


def controller_choice():
    """
    This function take a choice of player who will start a game
    """
    while True:
        choice = input('Ваш выбор: ')
        if choice.lower() == 'выход':
            exit_from_game()
        if (choice.lower() == 'я') or (choice.lower() == 'компьютер'):
            break
        elif choice.lower() == 'города':
            print('Ещё не был введён ни один город.')
        else:
            print('Не нарушайте правила, введите "я" или "компьютер" :(')
    print()
    return choice


def names_of_used_cities():
    """
    This function print used cities
    """
    global used_cities
    with open("logs.txt", 'a', encoding='utf-8') as logs:
        logs.write("{} Function cities was called\n".format((datetime.now().strftime("%d-%m-%Y %H:%M"))))
    print('Были использованы города:', end=' ')
    for name in used_cities:
        if name == used_cities[-1]:
            print(name, end='.')
        else:
            print(name, end=', ')
    print()


def exit_from_game():
    """
    This function realize a exit from game
    """
    with open("logs.txt", 'a', encoding='utf-8') as logs:
        logs.write("{} Game ended \n".format((datetime.now().strftime("%d-%m-%Y %H:%M"))))
    print()
    print('**************************************************\n'
          ' Спасиибо за игру!  \n'
          ' До скорой встречи! \n'
          ' Вы назвали', score_player, 'город(-а, -ов)\n'
          ' Компьютер назвал', score_computer, 'город(-а, -ов)\n'
          '**************************************************\n')
    exit()


cities = []
with open('cities.txt', encoding='utf-8') as file:
    for line in file:
        cities.append(line[0:-2])
exceptions_symbols = ['ъ', 'ь', 'ы']
used_cities = []
score_player = 0
score_computer = 0
with open("logs.txt", 'a', encoding='utf-8') as logs:
    logs.write("{} Game started \n".format((datetime.now().strftime("%d-%m-%Y %H:%M"))))
starter_menu()
while True:
    command = controller_command()
    print()
    if command.lower() == 'выход':
        exit_from_game()
    if command.lower() == 'правила':
        rules()
    if command.lower() == 'начать':
        print('Давайте начнём :)\n'
              'Кто будет начинать первым? Вы или компьютер?\n'
              'Если вы, то введите "я", если компьютер - "компьютер"\n'
              'Также вы можете в любой момент увидеть список названных городов.\n'
              'Для этого введите "города"')
        choice = controller_choice()
        if choice.lower() == 'я':
            with open("logs.txt", 'a', encoding='utf-8') as logs:
                logs.write("{} Game started by player\n".format((datetime.now().strftime("%d-%m-%Y %H:%M"))))
            last_letter = first_town()
            next_town = next_word(last_letter)
            while True:
                if next_town is None:
                    exit('Компьютер проиграл.\n'
                         'Победа за вами! Вы знаете очень много городов :)')
                last_letter = player_move(find_last_letter(next_town))
                next_town = next_word(last_letter)
        if choice.lower() == 'компьютер':
            with open("logs.txt", 'a', encoding='utf-8') as logs:
                logs.write("{} Game started by computer\n".format((datetime.now().strftime("%d-%m-%Y %H:%M"))))
            random_index_of_city = randrange(len(cities))
            next_town = cities[random_index_of_city]
            print('Город компьютера: ' + next_town)
            cities.remove(next_town)
            used_cities.append(next_town)
            score_computer += 1
            with open("logs.txt", 'a', encoding='utf-8') as logs:
                logs.write("{} Next word from computer - {} \n".format((datetime.now().strftime("%d-%m-%Y %H:%M")),
                                                                       next_town))
            print('Вам нужно написать город на букву "' + find_last_letter(next_town).upper() + '"')
            while True:
                if next_town is None:
                    exit('Компьютер проиграл.\n'
                         'Победа за вами! Вы знаете очень много городов :)')
                last_letter = player_move(find_last_letter(next_town))
                next_town = next_word(last_letter)
