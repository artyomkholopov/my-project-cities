import telebot
from telebot import types
from random import randrange
from datetime import datetime
import time

token = '1123519088:AAF97OFQmXU-PnZA4QQwcaXm5t9pj3omsXA'

bot = telebot.TeleBot(token)

cities = []
with open('cities.txt', encoding='utf-8') as file:
    for line in file:
        cities.append(line[0:-2])
exceptions_symbols = ['ъ', 'ь', 'ы']
used_cities = []
score_player = 0
score_computer = 0
last_letter_ = ""

def first_town(message):
    global cities, used_cities, score_player
    word = message.text
    log(message)
    if word == '/restart':
        bot.send_message(message.chat.id, 'Перезапуск\n'
                                          'Бот назвал ' + str(score_computer) + ' город(-а, -ов)\n'
                                          'Вы назвали ' + str(score_player) + ' город(-а, -ов)')
        welcome(message)
        return
    word = word.lower()
    word = word.title()
    if word.lower() == 'города':
        bot.register_next_step_handler(bot.send_message(message.chat.id,
                                                        'Ещё не был введён ни один город. Повторите попытку'),
                                       first_town)
    elif word in cities:
        cities.remove(word)
        used_cities.append(word)
        score_player += 1
        bot.send_message(message.chat.id, 'Хорошо! Такой город существует.')
        find_last_letter(word, message)
    else:
        bot.register_next_step_handler(bot.send_message(message.chat.id,
                                                        'Такого города не существует. Повторите попытку.'), first_town)

def find_last_letter(word, message):
    global exceptions_symbols, last_letter_
    if word[-1] in exceptions_symbols:
        last_letter_ = word[-2]
    else:
        last_letter_ = word[-1]
    bot.send_message(message.chat.id, 'Последнняя буква вашего города: ' + last_letter_)
    next_word(message.chat.id)


def names_of_used_cities(message):
    global used_cities
    print_used_cities = []
    for name in used_cities:
        print_used_cities.append(name)
    bot.send_message(message.chat.id, 'Были использованы города: ' + " ".join(used_cities))


def player_move(message):
    global cities, used_cities, score_player, last_letter_
    town = message.text
    town = town.lower()
    town = town.title()
    log(message)
    if town.lower() == 'города':
        bot.register_next_step_handler(bot.send_message(message.chat.id, 'Были использованы города: ' + ", ".join(used_cities)), player_move)
    elif town.lower() == '/restart':
        bot.send_message(message.chat.id, 'Перезапуск\n'
                                          'Бот назвал ' + str(score_computer) + ' город(-а, -ов)\n'
                                          'Вы назвали ' + str(score_player) + ' город(-а, -ов)')
        welcome(message)
        return
    elif town in cities and last_letter_ == town[0].lower():
        global exceptions_symbols
        if town[-1] in exceptions_symbols:
            last_letter_ = town[-2]
        else:
            last_letter_ = town[-1]
        cities.remove(town)
        used_cities.append(town)
        score_player += 1
        bot.send_message(message.chat.id, 'Хорошо! Такой город существует.')
        next_word(message.chat.id)
    elif town in cities and last_letter_ != town[0].lower():
        bot.register_next_step_handler(bot.send_message(message.chat.id, 'Такой город существует,'
                                                                         ' но он начинается не на последнюю букву'
                                                                         ' предыдущего города.\n Повторите попытку.'),
                                       player_move)
    else:
        bot.register_next_step_handler(bot.send_message(message.chat.id, 'Такого города не существует(возможно'
                                                                         ' вы его уже называли). Повторите попытку.'),
                                       player_move)


def next_word(chat_id):
    global cities, used_cities, score_computer, last_letter_
    for town in cities:
        if town[0].lower() == last_letter_:
            first_index = cities.index(town)
            break
    for town in cities:
        if town[0].lower() == chr(ord(last_letter_) + 1):
            second_index = cities.index(town)
            break
    random_index = randrange(first_index, second_index)
    town = cities[random_index]
    global exceptions_symbols
    if town[-1] in exceptions_symbols:
        last_letter_ = town[-2]
    else:
        last_letter_ = town[-1]
    cities.remove(town)
    used_cities.append(town)
    score_computer += 1
    bot.register_next_step_handler(bot.send_message(chat_id, 'Город бота: ' + town +
                                                    '\n Вам нужно назвать город на букву "' + last_letter_.upper() + '"'),
                                   player_move)

def start_game(message):
    global cities
    first_town(message)
    #bot.send_message(message.chat.id, last_letter)

def log(message):
    from datetime import datetime
    log_tg = str(datetime.now())
    log_tg += " Сообщение от {0} {1} (id = {2}) {3} \n".format(message.from_user.first_name,
                                                              message.from_user.last_name,
                                                              str(message.from_user.id), message.text)
    with open(file='logs.txt', mode= 'a', encoding='utf-8') as logs:
        logs.write(log_tg)

def call_log_player(call):
    from datetime import datetime
    log_tg = str(datetime.now())
    log_tg += " Сообщение от {0} {1} (id = {2}) {3} \n".format(call.message.from_user.first_name,
                                                              call.message.from_user.last_name,
                                                              str(call.message.from_user.id), call.message.text)
    with open(file='logs.txt', mode= 'a', encoding='utf-8') as logs:
        logs.write(log_tg)

def call_log(call, cmd):
    from datetime import datetime
    log_tg = str(datetime.now())
    log_tg += " Сообщение от {0} {1} (id = {2}) {3} \n".format(call.message.from_user.first_name,
                                                              call.message.from_user.last_name,
                                                              str(call.message.from_user.id), cmd)
    with open(file='logs.txt', mode= 'a', encoding='utf-8') as logs:
        logs.write(log_tg)

@bot.message_handler(commands=['start'])
def welcome(message):
    global exceptions_symbols, used_cities, score_player, score_computer, last_letter_
    cities = []
    with open('cities.txt', encoding='utf-8') as file:
        for line in file:
            cities.append(line[0:-2])
    exceptions_symbols = ['ъ', 'ь', 'ы']
    used_cities = []
    score_player = 0
    score_computer = 0
    last_letter_ = ""
    markup = types.InlineKeyboardMarkup()
    button1 = types.InlineKeyboardButton(text='Правила', callback_data='правила')
    button2 = types.InlineKeyboardButton(text='Играть', callback_data='играть')
    markup.add(button1, button2)
    log_tg = str(datetime.now())
    log(message)
    log_tg += ' Сообщение от Game "Cities" None (id = 1123519088) Бот запущен \n'
    with open(file='logs.txt', mode='a', encoding='utf-8') as logs:
        logs.write(log_tg)
    bot.send_message(message.chat.id, '<b>{0.first_name},  Добро пожаловать в игру "Города"</b>\n'
                                      'Для ознакомления с правилами игры нажмите на кнопку "Правила"\n'
                                      'Чтобы начать игру нажмите на кнопку "Играть"\n'.format(message.from_user, bot.get_me()),
                                      parse_mode='html', reply_markup=markup)


@bot.callback_query_handler(func=lambda call: True)
def rules_or_game(call):
    call_log_player(call)
    answer = ''
    if call.data == 'правила':
        answer =  'Правила игры "Города"    \n'
        answer += '*Начинается игра с любого названия города.\n'
        answer += '*Каждый из участников игры по "кругу" называет реальный город, название'
        answer += ' которого начинается на букву, которой заканчивается предыдущее'
        answer += ' название города\n'
        answer += '*Например: МосквА-АнапА-АгриджентО-ОмсК(и так далее)\n'
        answer += '*Запрещается повторение городов.\n'
        answer += '*Названий городов на "ё", "ы", твердый "ъ" и мягкий "ъ" знак нет.\n'
        answer += '*Игра завершается когда игрок не в силах '
        answer += 'больше вспомнить название города.'
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(text='Играть', callback_data='играть'))
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text=answer,
                              reply_markup=markup)
        call_log(call, cmd='Правила были выведены')
    elif call.data == 'играть':
        call_log_player(call)
        answer =  'Давайте начнём :)\n'
        answer += 'Кто будет начинать первым? Вы или бот?\n'
        answer += 'Если вы, то нажмите кнопку "я", если бот - кнопку "Бот"\n'
        answer += 'Также вы можете в любой момент увидеть список названных городов.\n'
        answer += 'Для этого во время игры введите "города"\n'
        answer += '<b>Для перезапуск бота введите  /restart</b>'
        markup = types.InlineKeyboardMarkup()
        button1 = types.InlineKeyboardButton(text='Я', callback_data='я')
        button2 = types.InlineKeyboardButton(text='Бот', callback_data='бот')
        markup.add(button1, button2)
        call_log(call, cmd='Игра началась')
        bot.edit_message_text(chat_id=call.message.chat.id,message_id=call.message.message_id, text=answer,
                              reply_markup=markup, parse_mode='html')
    elif call.data == 'я':
        call_log_player(call)
        call_log(call, cmd='Игра запущена с города игрока')
        bot.register_next_step_handler(
        bot.edit_message_text(text="Введите название города",chat_id=call.message.chat.id,
                              message_id=call.message.message_id), start_game)
    elif call.data == 'бот':
        call_log_player(call)
        call_log(call, cmd='Игра запущена с города бота')
        global cities, used_cities, score_computer, last_letter_
        random_index_of_city = randrange(len(cities))
        next_town = cities[random_index_of_city]
        global exceptions_symbols
        if next_town[-1] in exceptions_symbols:
            last_letter_ = next_town[-2]
        else:
            last_letter_ = next_town[-1]
        cities.remove(next_town)
        used_cities.append(next_town)
        score_computer += 1
        bot.register_next_step_handler(bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,text='Город компьютера: ' + next_town
                                                             + '\nВам нужно написать город на букву "' + last_letter_.upper() + '"'), player_move)

@bot.message_handler(commands=['restart'])
def restart(message):
    global exceptions_symbols, used_cities, score_player, score_computer,last_letter_
    cities = []
    with open('cities.txt', encoding='utf-8') as file:
        for line in file:
            cities.append(line[0:-2])
    exceptions_symbols = ['ъ', 'ь', 'ы']
    used_cities = []
    score_player = 0
    score_computer = 0
    last_letter_ = ""
    welcome

bot.polling(none_stop=True)
